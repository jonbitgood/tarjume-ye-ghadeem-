\id MIC
\h میکاه نبی
\toc1 کتاب میکاه نبی
\toc2 میکاه نبی
\c ۱
\p
\v ۱ کلام خداوند که بر میکاهِ مورَشَتی در ایّام یوتام و آحاز و حِزْقیا، پادشاهان یهودا نازل شد و آن را دربارهٔ سامره و اورشلیم دید.
\v ۲ ای جمیع قوم‌ها بشنوید و ای زمین و هر چه در آن است گوش بدهید، و خداوند یهوه یعنی خداوند از هیکل قدسش بر شما شاهد باشد.
\v ۳ زیرا اینک خداوند از مکان خود بیرون می‌آید و نزول نموده، بر مکان‌های بلند زمین می‌خرامد.
\v ۴ و کوهها زیر او گداخته می‌شود و وادیها مُنْشَقّ می‌گردد، مثل موم پیش آتش و مثل آب که به نشیب ریخته شود.
\v ۵ این همه به سبب عصیان یعقوب و گناه خاندان اسرائیل است. عصیان یعقوب چیست؟ آیا سامره نیست؟ و مکان‌های بلند یهودا چیست؟ آیا اورشلیم نمی‌باشد؟
\v ۶ پس سامره را به توده سنگ صحرا و مکان غَرس نمودن مَوْها مبدّل خواهم ساخت و سنگهایش را به درّه ریخته، بنیادش را منکشف خواهم نمود.
\v ۷ و همه بتهای تراشیده شده آن خرد و همه مزدهایش به آتش سوخته خواهد شد و همه تماثیلش را خراب خواهم کرد زیرا که از مزد فاحشه آنها را جمع کرد و به مزد فاحشه خواهد برگشت.
\v ۸ به این سبب ماتم گرفته، وِلْوَلَه خواهم نمود و برهنه و عریان راه خواهم رفت و مثل شغالها ماتم خواهم گرفت و مانند شتر مرغهانوحه‌گری خواهم نمود.
\v ۹ زیرا که جراحت‌های وی علاج‌پذیر نیست چونکه به یهودا رسیده و به دروازه‌های قوم من یعنی به اورشلیم داخل گردیده است.
\v ۱۰ در جَتّ خبر مرسانید و هرگز گریه منمایید. در خانه عَفْرَه، در غبار خویشتن را غلطانیدم.
\v ۱۱ ای ساکنه شافیر عریان و خجل شده، بگذر. ساکنه صأنان بیرون نمی‌آید. ماتم بیتْاِیصَلْ مکانش را از شما می‌گیرد.
\v ۱۲ زیرا که ساکنه ماروت به جهت نیکویی درد زه می‌کشد، چونکه بلا از جانب خداوند به دروازه اورشلیم فرود آمده است.
\v ۱۳ ای ساکنه لاکیش اسب تندرو را به ارابه ببند. او ابتدای گناه دختر صَهیون بود، چونکه عصیان اسرائیل در تو یافت شده است.
\v ۱۴ بنابراین طلاق نامه‌ای به مُورَشَتْ جَتّ خواهی داد. خانه‌های اَکْذِیب، چشمه فریبنده برای پادشاهان اسرائیل خواهد بود.
\v ۱۵ ای ساکنه مَریشَه بار دیگر مالکی بر تو خواهم آورد. جلال اسرائیل تا به عَدُلاّم خواهد آمد.
\v ۱۶ خویشتن را برای فرزندان نازنین خود گَرْ ساز و موی خود را بتراش. گری سر خود را مثل کرکس زیاد کن زیرا که ایشان از نزد تو به اسیری رفته‌اند.
\c ۲
\p
\v ۱ وای بر آنانی که بر بسترهای خود ظلم را تدبیر می‌نمایند و مرتکب شرارت می‌شوند. در روشنایی صبح آن را بجا می‌آورند، چونکه در قوّت دست ایشان است.
\v ۲ بر زمینها طمع می‌ورزند و آنها را غصب می‌نمایند و بر خانه‌ها نیز و آنها را می‌گیرند و بر مرد و خانه‌اش و شخص و میراثش ظلم می‌نمایند.
\v ۳ بنابراین خداوند چنین می‌گوید، هان من بر این قبیله بلایی را تدبیر می‌نمایم که شما گردن خود را از آن نتوانید بیرون آورد و متکبّرانه نخواهید خرامید زیرا که آن زمان زمان بد است.
\v ۴ در آن روز بر شما مَثَل خواهند زد و مرثیه سوزناک خواهند خواند و خواهند گفت بالّکل هلاک شده‌ایم. نصیب قوم مرا به دیگران داده است. چگونه آن را از من دور می‌کند و زمینهای مرا به مرتدّان تقسیم می‌نماید.
\v ۵ بنابراین برای تو کسی نخواهد بود که ریسمان را به قرعه در جماعت خداوند بکَشَدْ.
\v ۶ ایشان نبوّت کرده، می‌گویند نبوّت مکنید. اگر به اینها نبوّت ننمایند، رسوایی دور نخواهد شد.
\v ۷ ای که به خاندان یعقوب مسمّی هستی آیا روح خداوند قاصر شده است و آیا اینها اعمال او می‌باشد؟ آیا کلام من برای هر که به استقامت سالک می‌باشد، نیکو نیست؟
\v ۸ لکن قوم من در این روزها به دشمنی برخاسته‌اند. شما ردا را از رخت آنانی که به اطمینان می‌گذرند و از جنگ روگردانند، می‌کَنید.
\v ۹ و زنان قوم مرا از خانه‌های مرغوب ایشان بیرون می‌کنید و زینت مرا از اطفال ایشان تا به ابد می‌گیرید.
\v ۱۰ برخیزید و بروید زیرا که این آرامگاه شما نیست چونکه نجس شده است. شما را به هلاکت سخت هلاک خواهد ساخت.
\v ۱۱ اگر کسی به بطالت و دروغ سالک باشد و کاذبانه گوید که من برای تو دربارۀشراب و مُسْکِرات نبوّت خواهم نمود، هرآینه او نبّی این قوم خواهد بود.
\v ۱۲ ای یعقوب، من البتّه تمامی اهل تو را جمع خواهم نمود و بقیه اسرائیل را فراهم آورده، ایشان را مثل گوسفندان بُصْرَه در یک جا خواهم گذاشت. ایشان مثل گله‌ای که در آغل خود باشد، به سبب کثرت مردمان غوغا خواهند کرد.
\v ۱۳ رخنه کننده پیش روی ایشان برآمده است. ایشان رخنه نموده و از دروازه عبور کرده، از آن بیرون رفته‌اند و پادشاه ایشان پیش روی ایشان و خداوند بر سر ایشان پیش رفته است.
\c ۳
\p
\v ۱ و گفتم، ای رؤسای یعقوب و ای داوران خاندان اسرائیل بشنوید! آیا بر شما نیست که انصاف را بدانید؟
\v ۲ آنانی که از نیکویی نفرت دارند و بر بدی مایل می‌باشند و پوست را از تن مردم و گوشت را از استخوانهای ایشان می‌کَنَنْد،
\v ۳ و کسانی که گوشت قوم مرا می‌خورند و پوست ایشان را از تن ایشان می‌کنند و استخوانهای ایشان را خُرد کرده، آنها را گویا در دیگ و مثل گوشت در پاتیل می‌ریزند،
\v ۴ آنگاه نزد خداوند استغاثه خواهند نمود و ایشان را اجابت نخواهد نمود، بلکه روی خود را در آنزمان از ایشان خواهد پوشانید چونکه مرتکب اعمال زشت شده‌اند.
\v ۵ خداوند دربارهٔ انبیایی که قوم مرا گمراه می‌کنند و به دندانهای خود می‌گزند و سلامتی را ندا می‌کنند، و اگر کسی چیزی به دهان ایشان نگذارد با او تدارک جنگ می‌بینند، چنین می‌گوید،
\v ۶ از این جهت برای شما شب خواهد بود که رؤیا نبینید و ظلمت برای شما خواهد بودکه فالگیری ننمایید. آفتاب بر انبیاء غروب خواهد کرد و روز بر ایشان تاریک خواهد شد.
\v ۷ و راییان خجل و فالگیران رسوا شده، جمیع ایشان لبهای خود را خواهند پوشانید چونکه از جانب خدا جواب نخواهد بود.
\v ۸ و لیکن من از قوّت روح خداوند و از انصاف و توانایی مملّو شده‌ام تا یعقوب را از عصیان او و اسرائیل را از گناهش خبر دهم.
\v ۹ ای رؤسای خاندان یعقوب و ای داوران خاندان اسرائیل این را بشنوید! شما که از انصاف نفرت دارید و تمامی راستی را منحرف می‌سازید،
\v ۱۰ و صهیون را به خون و اورشلیم را به ظلم بنا می‌نمایید،
\v ۱۱ رؤسای ایشان برای رشوه داوری می‌نمایند و کاهنان ایشان برای اجرت تعلیم می‌دهند و انبیای ایشان برای نقره فال می‌گیرند و بر خداوند توکّل نموده، می‌گویند، آیا خداوند در میان ما نیست؟ پس بلا به ما نخواهد رسید.
\v ۱۲ بنابراین صَهْیون به سبب شما مثل مزرعه شیار خواهد شد و اورشلیم به توده‌های سنگ و کوه خانه به بلندیهای جنگل مبدّل خواهد گردید.
\c ۴
\p
\v ۱ و در ایّام آخر، کوهِ خانه خداوند بر قلّه کوهها ثابت خواهد شد و بر فوق تلّها برافراشته خواهد گردید و قوم‌ها بر آن روان خواهند شد.
\v ۲ و امّت‌های بسیار عزیمت کرده، خواهند گفت، بیایید تا به کوه خداوند و به خانه خدای یعقوب برآییم تا طریق‌های خویش را به ما تعلیم دهد و به راههای وی سلوک نماییم زیرا که شریعت از صهیون و کلام خداوند از اورشلیم‌صادر خواهد شد.
\v ۳ و او در میان قوم‌های بسیار داوری خواهد نمود و امّت‌های عظیم را از جای دور تنبیه خواهد کرد و ایشان شمشیرهای خود را برای گاوآهن و نیزه‌های خویش را برای ارّه‌ها خواهند شکست و امّتی بر امّتی شمشیر نخواهد کشید و بار دیگر جنگ را نخواهند آموخت.
\v ۴ و هر کس زیر مَوِ خود و زیر انجیر خویش خواهد نشست و ترساننده‌ای نخواهد بود زیرا که دهان یهوه صبایوت تکلّم نموده است.
\v ۵ زیرا که جمیع قوم‌ها هر کدام به اسم خدای خویش سلوک می‌نمایند امّا ما به اسم یهوه خدای خود تا ابدالآباد سلوک خواهیم نمود.
\v ۶ خداوند می‌گوید که در آن روز لنگان را جمع خواهم کرد و رانده شدگان و آنانی را که مبتلا ساخته‌ام فراهم خواهم آورد.
\v ۷ و لنگان را بقیتی و دور شدگان را قوم قوّی خواهم ساخت و خداوند در کوه صَهْیوُن برایشان از الآن تا ابدالآباد سلطنت خواهد نمود.
\v ۸ و تو ای برج گله و ای کوه دختر صَهیون این به تو خواهد رسید و سلطنت اوّل یعنی مملکت دختر اورشلیم خواهد آمد.
\v ۹ الآن چرا فریاد برمی‌آوری؟ آیا در تو پادشاهی نیست و آیا مُشیر تو نابود شده است که دردْ تو را مثل زنی که می‌زاید گرفته است؟
\v ۱۰ ای دختر صَهیون مثل زنی که می‌زاید درد زه کشیده، وضع حمل نما زیرا که الآن از شهر بیرون رفته، در صحرا خواهی نشست و به بابل رفته، در آنجا رهایی خواهی یافت و در آنجا خداوند تو را از دست دشمنانت رهایی خواهد داد.
\v ۱۱ و الآن امّت‌های بسیار بر تو جمع شده، می‌گویند که صهیون نجس خواهد شد و چشمان ما بر اوخواهد نگریست.
\v ۱۲ امّا ایشان تدبیرات خداوند را نمی‌دانند و مشورت او را نمی‌فهمند زیرا که ایشان را مثل بافه‌ها در خرمنگاه جمع کرده است.
\v ۱۳ ای دختر صهیون برخیز و پایمال کن زیرا که شاخ تو را آهن خواهم ساخت و سمهای تو را برنج خواهم نمود و قوم‌های بسیار را خواهی کوبید و حاصل ایشان را برای یهوه و دولت ایشان را برای خداوند تمامی زمین وقف خواهی نمود.
\c ۵
\p
\v ۱ ای دخترِ افواج، الآن در فوجها جمع خواهی شد! ایشان به ضدّ ما سنگرها بسته‌اند. با عصا بر رخسار داور اسرائیل خواهند زد.
\v ۲ و تو ای بَیتْلَحَمِاَفْراتَه اگر چه در هزاره‌های یهودا کوچک هستی، از تو برای من کسی بیرون خواهد آمد که بر قوم من اسرائیل حکمرانی خواهد نمود و طلوع‌های او از قدیم و از ایّام ازل بوده است.
\v ۳ بنابراین ایشان را تا زمانی که زن حامله بزاید تسلیم خواهد نمود و بقیه برادرانش با بنی‌اسرائیل بازخواهند گشت.
\v ۴ و او خواهد ایستاد و در قوّت خداوند و در کبریایی اسم یهوه خدای خویش (گله خود را) خواهد چرانید و ایشان به آرامی ساکن خواهند شد زیرا که او الآن تا اقصای زمین بزرگ خواهد شد.
\v ۵ و او سلامتی خواهد بود. هنگامی که آشور به زمین ما داخل شده، بر قصرهای ما قدم نهد، آنگاه هفت شبان و هشت سرورِ آدمیان را به مقابل او برپا خواهیم داشت.
\v ۶ و ایشان زمین آشور و مدخل‌های زمین نِمْرُود را با شمشیر حکمرانی خواهند نمود و او ما را از آشور رهایی خواهد داد، هنگامی که به زمین ما داخل شده، حدود ما را پایمال کند.
\v ۷ و بقیه یعقوب در میان قوم‌های بسیار مثل شبنم از جانب خداوند خواهد بود و مانند بارشی که بر گیاه می‌آید که برای انسان انتظار نمی‌کشد و به جهت بنی‌آدم صبر نمی‌نماید.
\v ۸ و بقیه یعقوب در میان امّت‌ها و در وسط قوم‌های بسیار، مثل شیر در میان جانوران جنگل و مانند شیر ژیان در میان گله‌های گوسفندان خواهند بود که چون عبور می‌نماید، پایمال می‌کند و می‌دَرَد و رهاننده‌ای نمی‌باشد.
\v ۹ و دست تو بر خصمانت بلند خواهد شد و جمیع دشمنانت منقطع خواهند گردید.
\v ۱۰ و خداوند می‌گوید که در آن روز اسبان تو را از میانت منقطع و ارابه‌هایت را معدوم خواهم نمود.
\v ۱۱ و شهرهای ولایت تو را خراب نموده، همه قلعه‌هایت را منهدم خواهم ساخت.
\v ۱۲ و جادوگری را از دست تو تلف خواهم نمود که فالگیران دیگر در تو یافت نشوند.
\v ۱۳ و بتهای تراشیده و تمثالهای تو را از میانت نابود خواهم ساخت که بار دیگر به صنعت دست خود سجده ننمایی.
\v ۱۴ و اَشیره‌هایت را از میانت کَنده، شهرهایت را منهدم خواهم ساخت.
\v ۱۵ و با خشم و غضب از امّت‌هایی که نمی‌شنوند انتقام خواهم کشید.
\c ۶
\p
\v ۱ آنچه خداوند می‌گوید بشنوید! برخیز و نزد کوهها مخاصمه نما و تلّها آواز تو رابشنوند.
\v ۲ ای کوهها مخاصمه خداوند را بشنوید و ای اساسهای جاودانی زمین! زیرا خداوند را با قوم خود مخاصمه‌ای است و با اسرائیل محاکمه خواهد کرد.
\v ۳ ای قوم من به تو چه کرده‌ام و به چه چیز تو را خسته ساخته‌ام؟ به ضدّ من شهادت بده.
\v ۴ زیرا که تو را از زمین مصر برآوردم و تو را از خانه بندگی فدیه دادم و موسی و هارون و مریم را پیش روی تو ارسال نمودم.
\v ۵ ای قوم من آنچه را که بالاق پادشاه موآب مشورت داد و آنچه بَلْعام‌بن بَعور او را جواب فرستاد، بیاد آور و آنچه را که از شِطّیم تا جِلْجال (واقع شد به خاطر دار) تا عدالت خداوند را بدانی.
\v ۶ با چه چیز به حضور خداوند بیایم و نزد خدای تعالی رکوع نمایم؟ آیا با قربانی‌های سوختنی و با گوساله‌های یک ساله به حضور وی بیایم؟
\v ۷ آیا خداوند از هزارها قوچ و از ده هزارها نهر روغن راضی خواهد شد؟ آیا نخست‌زاده خود را به عوض معصیتم و ثمره بدن خویش را به عوض گناه جانم بدهم؟
\v ۸ ای مرد از آنچه نیکو است تو را اخبار نموده است؛ و خداوند از تو چه چیز را می‌طلبد غیر از اینکه انصاف را بجا آوری و رحمت را دوست بداری و در حضور خدای خویش با فروتنی سلوک نمایی؟
\v ۹ آواز خداوند به شهر ندا می‌دهد و حکمتْ اسم او را مشاهده می‌نماید. عصا و تعیین کننده آن را بشنوید.
\v ۱۰ آیا تا به حال گنجهای شرارت و ایفای ناقصِ ملعون در خانه‌شریران می‌باشد؟
\v ۱۱ آیا من با میزانهای شرارت و با کیسه سنگهای ناراست بری خواهم شد؟
\v ۱۲ زیرا که دولتمندانِ او از ظلم ممّلواند و ساکنانش دروغ می‌گویند و زبان ایشان در دهانشان فریب محض است.
\v ۱۳ پس من نیز تو را به سبب گناهانت به جراحات مهلک مجروح ساخته، خراب خواهم نمود.
\v ۱۴ تو خواهی خورد امّا سیر نخواهی شد و گرسنگی تو در اندرونت خواهد ماند و بیرون خواهی برد امّا رستگار نخواهی ساخت و آنچه را که رستگار نمایی من به شمشیر تسلیم خواهم نمود.
\v ۱۵ تو خواهی کاشت امّا نخواهی دروید؛ تو زیتون را به پا خواهی فشرد امّا خویشتن را به روغن تدهین نخواهی نمود؛ و عصیر انگور را امّا شراب نخواهی نوشید.
\v ۱۶ زیرا که قوانین عُمْرِی و جمیع اعمال خاندان اَخْآب نگاه داشته می‌شود و به مشورت‌های ایشان سلوک می‌نمایید تا تو را به ویرانی و ساکنانش را به سخّریه تسلیم نمایم، پس عار قوم مرا متحمّل خواهید شد.
\c ۷
\p
\v ۱ وای بر من زیرا که مثل جمع کردن میوه‌ها و مانند چیدن انگورهایی شده‌ام که نه خوشه‌ای برای خوراک دارد و نه نوبر انجیری که جان من آن را می‌خواهد.
\v ۲ مرد مُتّقی از جهان نابود شده، و راست کردار از میان آدمیان معدوم گردیده است. جمیع ایشان برای خون کمین می‌گذارند و یکدیگر را به دام صید می‌نمایند.
\v ۳ دستهای ایشان برای شرارت چالاک است؛ رئیس طلب می‌کند و داور رشوه می‌خواهد و مردبزرگ به هوای نفس خود تکلّم می‌نماید؛ پس ایشان آن را به هم می‌بافند.
\v ۴ نیکوترین ایشان مثل خار می‌باشد و راست کردار ایشان از خاربست بدتر. روز پاسبانانت و (روز) عقوبت تو رسیده است، الآن اضطراب ایشان خواهد بود.
\v ۵ بر یار خود اعتماد مدار و بر دوست خالص خویش توکّل منما و درِ دهان خود را از هم آغوش خود نگاه دار.
\v ۶ زیرا که پسر، پدر را افتضاح می‌کند و دختر با مادر خود و عروس با خارسوی خویش مقاومت می‌نمایند و دشمنان شخص اهل خانه او می‌باشند.
\v ۷ امّا من بسوی خداوند نگرانم و برای خدای نجات خود انتظار می‌کشم و خدای من مرا اجابت خواهد نمود.
\v ۸ ای دشمن، من بر من شادی منما زیرا اگر چه بیفتم خواهم برخاست و اگرچه در تاریکی بنشینم، خداوند نور من خواهد بود.
\v ۹ غضب خداوند را متحمّل خواهم شد زیرا به او گناه ورزیده‌ام تا او دعوی مرا فیصل کند و داوری مرا بجا آورد. پس مرا به روشنایی بیرون خواهد آورد و عدالت او را مشاهده خواهم نمود.
\v ۱۰ دشمنم این را خواهد دید و خجالت او را خواهد پوشانید زیرا به من می‌گوید، یهوه خدای تو کجا است؟ چشمانم بر او خواهد نگریست و او الآن مثل گِل کوچه‌ها پایمال خواهد شد.
\v ۱۱ در روز بنا نمودن دیوارهایت در آن روز شریعت‌دور خواهد شد.
\v ۱۲ در آن روز از آشور و از شهرهای مصر و از مصر تا نهر (فرات) و از دریا تا دریا و از کوه تا کوه نزد تو خواهند آمد.
\v ۱۳ و زمین به سبب ساکنانش، به جهت نتیجه اعمالشان ویران خواهد شد.
\v ۱۴ قوم خود را به عصای خویش شبانی کن و گوسفندان میراث خود را که در جنگل و در میان کَرْمَلْ به تنهایی ساکن می‌باشند. ایشان مثل ایّام سابق در باشان و جِلْعاد بچرند.
\v ۱۵ مثل ایّامی که از مصر بیرون آمدی کارهای عجیب به او نشان خواهم داد.
\v ۱۶ امّت‌ها چون این را بینند، از تمامی توانایی خویش خجل خواهند شد و دست بر دهان خواهند گذاشت و گوشهای ایشان کر خواهد شد.
\v ۱۷ مثل مار خاک را خواهند لیسید و مانند حشراتِ زمین از سوراخهای خود با لرزه بیرون خواهند آمد و بسوی یهوه خدای ما با خوف خواهند آمد و از تو خواهند ترسید.
\v ۱۸ کیست خدایی مثل تو که عصیان را می‌آمرزد و از تقصیر بقیه میراث خویش درمی‌گذرد. او خشم خود را تا به ابد نگاه نمی‌دارد زیرا رحمت را دوست می‌دارد.
\v ۱۹ او باز رجوع کرده، بر ما رحمت خواهد نمود و عصیان ما را پایمال خواهد کرد و تو جمیع گناهان ایشان را به عمق‌های دریا خواهی انداخت.
\v ۲۰ امانت را برای یعقوب و رأفت را برای ابراهیم بجا خواهی آورد چنانکه در ایّام سَلَف برای پدران ما قَسَم خوردی.
